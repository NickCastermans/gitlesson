public interface IScoreManager {

    void onRightAnswer();

    void onWrongAnswer();

    /**
     * Use when answer is partially right.
     * @param score ranges from 0 to 1.
     */
    default void onPartialAnswer(float score) {
        if(score > 0.5)
            onRightAnswer();
        else
            onWrongAnswer();
    }

    boolean isGameOver();

    int getScore();

}
