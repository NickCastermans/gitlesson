import java.util.Scanner;

public class KeyBoardInput {

    private Scanner scanner;
    private static KeyBoardInput instance;

    private KeyBoardInput() {

    }

    public static KeyBoardInput getInstance() {
        if (instance == null) {
            instance = new KeyBoardInput();
        }
        return instance;
    }

    public Scanner openKeyboard() {
        if (!isActive()) {
            this.scanner = new Scanner(System.in);
        }
        return this.scanner;
    }

    public void closeKeyboard() {
        if (isActive()) {
            this.scanner.close();
            this.scanner = null;
        }
    }

    public String getInput() {
        if (!isActive()) {
            openKeyboard();
        } return this.scanner.nextLine();
    }

    public String getInputAndCloseScan() {
        if (!isActive()) {
            openKeyboard();
        }
        String returnValue = this.scanner.nextLine();
        closeKeyboard();
        return returnValue;
    }

    public boolean isActive() {
        return this.scanner!=null;
    }
}
