import java.util.Arrays;
import java.util.List;

public class QuestionService {
    private List<Question> questions;

    public QuestionService() {
        //init questions
        questions = Arrays.asList(new Question(
                "Which fictional city is the home of Batman?",
                "Gotham City."
        ),new Question(
                        "Which fictional city is the home of Batman?",
                        "Gotham City"
                ),new Question(
                        "Spinach is high in which mineral?",
                        "Iron"
                ),new Question(
                        "Traditionally, how many Wonders of the World are there?",
                        "Seven"
                ),new Question(
                        "Which planet is the closest to Earth?",
                        "Venus"
                ),new Question(
                        "Mount Everest is found in which mountain range?",
                        "The Himalayas"
                ),new Question(
                        "How many sides does an octagon have?",
                        "8"
                ),new Question(
                        "What is the name of the city where the cartoon family The Simpsons live?",
                        "Springfield"
                )
                );
    }

    public Question getQuestion(){
        return questions.get(getRandomNumber());
    }

    private  int getRandomNumber(){
        int x =  (int)(Math.random() * ((this.questions.size())));
        return x;
    }
}
