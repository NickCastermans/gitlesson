public class ScoreMaxGuesses implements IScoreManager {

    private final int maxGuesses;
    private int guessesLeft;
    private int score;

    public ScoreMaxGuesses(int maxGuesses) {
        this.maxGuesses = maxGuesses;
        this.guessesLeft = maxGuesses;
        this.score = 0;
    }

    @Override
    public void onRightAnswer() {}

    @Override
    public void onWrongAnswer() {
        guessesLeft -= 1;
    }

    @Override
    public boolean isGameOver() {
        return guessesLeft <= 0;
    }

    @Override
    public int getScore() {
        return guessesLeft;
    }
}
