public class Main {

    public static void main(String[] args) {
        KeyBoardInput keyboard = KeyBoardInput.getInstance();
        IScoreManager scoreManager = new ScoreMaxGuesses(3);
        QuestionService questionService = new QuestionService();

        while(!scoreManager.isGameOver()) {
            Question question = questionService.getQuestion();
            System.out.println(question.getQuestion());
            String answer = keyboard.getInput();
            if(answer.equals(question.getAnswer())) {
                scoreManager.onRightAnswer();
                System.out.println("Correct answer!");
            } else {
                scoreManager.onWrongAnswer();
                System.out.println("Wrong answer :(");
                System.out.printf("Answer was %s.\n", question.getAnswer());
            }
            System.out.printf("Lifes: %d\n", scoreManager.getScore());
        }
        keyboard.closeKeyboard();
        System.out.println("Game Over.");
    }

}
